import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

const App = () => {
    return (
        <>
            <Router>
                <Switch>
                    <Route exact path="/" component={() => <div>Home</div>} />
                    <Route exact path="/ingredients" component={() => <div>Ingredients</div>} />
                    <Route
                        exact
                        path="/bottles-saved"
                        component={() => <div>Bottles</div>}
                    />
                    <Route path="*" render={() => <Redirect to={{ pathname: '/' }} />} />
                </Switch>
            </Router>
            <style>{`
                html, body {
                position: absolute;
                width: 100%;
                height: 100%;
                }
                #root{
                    height: 100%;
                    min-height: 100%;
                }
                * {
                    box-sizing: border-box;
                }
                    body {
                    margin: 0;
                    font-family: "Open Sans";
                }
        `}</style>
        </>
    );
};

export default App;
