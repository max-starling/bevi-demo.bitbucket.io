import React from "react";
import ReactDOM from "react-dom";

import App from "./App.js";

const AppContainer = () => (
   <App />
);

ReactDOM.render(<AppContainer />, document.getElementById("root"));